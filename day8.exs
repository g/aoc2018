defmodule DayEight do
  def part_one(input) do
    parsed = parse_input(input)
  end

  def part_two(input) do
    parsed = parse_input(input)
  end

  def parse_input(input) do
    ints = String.replace(input, "\n", "")
           |> String.split(" ")
           |> Enum.map(&String.to_integer/1)
    parse_node(ints, 0)
  end

  def parse_node([], _), do: {[], 0}
  def parse_node([0, meta | tail], 0) do
    IO.puts "childless end node #{length tail}"
    {ntail, metas} = take(tail, meta, [])
    metasum = Enum.sum(metas)
    {ntail, metasum}
  end

  def parse_node([0, meta | tail], crem) do
    IO.puts "childless node #{length tail} crem: #{crem}"
    {ntail, metas} = take(tail, meta, [])
    {ptail, pmetasum} = parse_node(ntail, crem - 1)
    metasum = Enum.sum(metas)
    {ptail, metasum + pmetasum}
  end

  def parse_node([child, meta | tail], 0) do
    IO.puts "end node #{length tail}"
    {ctail, cmetasum} = parse_node(tail, child)
    {ntail, metas} = take(ctail, meta, [])
    metasum = Enum.sum(metas)
    {ntail, metasum + cmetasum}
  end

  def parse_node([child, meta | tail], crem) do
    IO.puts "regular node #{length tail} crem: #{crem}"
    {ctail, cmetasum} = parse_node(tail, child)
    {ntail, metas} = take(ctail, meta, [])
    metasum = Enum.sum(metas)
    {ntail, metasum + cmetasum}
  end

# def parse_node(left, crem) do
#   IO.inspect left ++ 0
#   parse_node([], crem - 1)
# end

  def take(rem, 0, acc), do: {rem, Enum.reverse acc}
#  def take([], num, acc), do: take([], num - 1, [0 | acc])
  def take([head | tail], num, acc) do
    take(tail, num - 1, [head | acc])
  end
end

{:ok, content} = File.read("input8.txt")

IO.puts("Input length: #{String.length content}")

IO.puts("Part 1: #{DayEight.part_one(content)}")
IO.puts("Part 2: #{DayEight.part_two(content)}")
