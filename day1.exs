defmodule DayOne do
  def part_one(input) do
    String.split(input, "\n") |> Enum.filter(&(&1 != ""))
    |> Enum.map(&DayOne.parse_num/1)
    |> List.foldr(0, &(&1 + &2))
  end

  def part_two(input),
    do: part_two(String.split(input, "\n") |> Enum.filter(&(&1 != "")) |> Enum.map(&DayOne.parse_num/1), 0, [])

  def part_two(input, acc, frequencies) do
    [head | tail] = input
    freq = head + acc
    # IO.puts("Got frequency #{freq}")
    if Enum.member?(frequencies, freq),
      do: freq,
      else: part_two(tail ++ [head], freq, [freq | frequencies])
  end

  def parse_num("+" <> num) do
    {intVal, ""} = Integer.parse(num)
    intVal
  end

  def parse_num("-" <> num) do
    {intVal, ""} = Integer.parse(num)
    0 - intVal
  end

  def parse_num(num) do
    IO.puts(num)
    0
  end
end

{:ok, content} = File.read("input1.txt")

IO.puts("Part 1: #{DayOne.part_one(content)}")
IO.puts("Part 2: #{DayOne.part_two(content)}")
