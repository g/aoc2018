defmodule DayTwo do
  def part_one(input) do
    lines = String.split(input, "\n") |> Enum.map(&String.codepoints/1)

    two =
      Enum.map(lines, fn string -> DayTwo.check_occurence(string, 2) end)
      |> Enum.filter(& &1)

    three =
      Enum.map(lines, fn string -> DayTwo.check_occurence(string, 3) end)
      |> Enum.filter(& &1)

    length(two) * length(three)
  end

  def check_occurence(line, req) do
    size =
      Enum.reduce(line, %{}, fn x, acc -> Map.update(acc, x, 1, &(&1 + 1)) end)
      |> Enum.into([])
      |> Enum.filter(fn {_, count} -> count == req end)
      |> length

    size > 0
  end

  def part_two(input) do
    lines = String.split(input, "\n") |> Enum.map(&String.codepoints/1)

    Enum.map(lines, fn line ->
      Enum.map(lines, fn linetwo ->
        diff = compare_strings(line, linetwo)

        if diff == 1 do
          IO.puts("#{line} #{linetwo}")
        end
      end)
    end)

    nil
  end

  def compare_strings(one, two) do
    Enum.zip(one, two)
    |> Enum.filter(fn {one, two} -> one != two end)
    |> length
  end
end
