# AoC 2018

This repo will contain my Advent of Code solutions and writeups (in markdown format) for AoC 2018. The writeups will also be available on my [website](https://gerd.tech/aoc2018).
