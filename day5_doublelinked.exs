defmodule DayFive do
  def part_one(input) do
    shortened = fully_react_new(input)
    # IO.puts shortened
    String.length(shortened) - 1
  end

  def part_two(input) do
    chars = Enum.map(?a..?z, &<<&1>>)

    [{_char, poly} | _] =
      Enum.map(chars, fn char -> {char, String.replace(input, char, "")} end)
      |> Enum.map(fn {char, poly} -> {char, String.replace(poly, String.upcase(char), "")} end)
      |> Enum.map(fn {char, poly} -> {char, fully_react_new(poly)} end)
      |> Enum.sort_by(fn {_char, poly} -> String.length(poly) end)

    # IO.puts poly
    String.length(poly) - 1
  end

  def react([<<char1>>, <<char2>> | tail], rev)
      when char1 == char2 - 32 or char2 == char1 - 32,
      do: react(tail, rev)

  def react([<<char1>> | tail], [<<char2>> | rev])
      when char1 == char2 - 32 or char2 == char1 - 32,
      do: react(tail, rev)

  def react([char | tail], rev), do: react(tail, [char | rev])
  def react("", rev), do: rev
  def react([], rev), do: rev

  def fully_react_new(input) do
    # IO.puts "mark"
    String.reverse(Enum.join(react(String.codepoints(input), [])))
  end
end

{:ok, content} = File.read("input5.txt")

IO.puts("Part 1: #{DayFive.part_one(content)}")
IO.puts("Part 2: #{DayFive.part_two(content)}")
